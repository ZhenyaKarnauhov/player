package ru.kev.game;

/**
 * Класс для запуска игры.
 *
 * @author Karnauhov Evgeniy.
 */
public class GameLauncher {
    public static void main(String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}
