package ru.kev.game;

/**
 * Класс для создания объекта типа Player.
 *
 * @author Karnauhov Evgeniy.
 */
public class Player {
    int number = 0;

    /**
     * Метод для генерирования случайного значения для переменной "Number".
     */

    public void guess() {
        number = (int) (Math.random() * 10);
    }

    /**
     * Метод для вывода предполагаемого числа.
     */
    public void writeNumber() {
        System.out.println("Я предполагаю" + number);
    }
}
